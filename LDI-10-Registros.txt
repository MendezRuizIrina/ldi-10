


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package registros;

/**
 *
 * @author Irina
 */
class Registros{

public static void main (String args []){
//Suma AH más AL y el valor sera asignado a AX en registro
int AH = 00101011;
int AL = 01101100;

int AX = AH&AL;

//Suma BH más BL y el valor sera asignado a BX en registro
int BH = 00111011;
int BL = 01111100;

int BX = BH&BL;

//Suma CH más CL y el valor sera asignado a CX en registro
int CH = 00101010;
int CL = 01101111;

int CX = CH&CL;

System.out.println(
        "---- Suma en AX ----\nLa suma de AH más AL es :"+Integer.toBinaryString(AX)+
                  "\n---- Suma en BX ----\nLa suma de BH más BL es:"+Integer.toBinaryString(BX)+
                           "\n---- Suma en CX ----\nLa suma de CH más CL es:"+Integer.toBinaryString(CX));
}
}